@extends('layouts.app')

@section('content')
<header>
  <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="/img/Pool.mp4" type="video/mp4">
    </video>
    <div class="container h-100">
      <div class="d-flex h-100 text-center align-items-center">
        <div class="w-100 text-white">
          <h1 class="display-4 font-weight-bold">Tecnoservice</h1>
          <p class="lead mb-0">La tecnologia al servizio del benessere</p>
          <a href="#" class="btn btn-primary mt-3">Chi siamo</a>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Services-->
  <section class="page-section my-5" id="services">
    <div class="container">
      <div class="text-center">
        <h2 class="section-heading text-uppercase font-weight-bold">I Nostri Servizi</h2>
        <h3 class="section-subheading text-muted mb-4">Lorem ipsum dolor sit amet consectetur.</h3>
      </div>
      <div class="row text-center my-5">
        <div class="col-md-3">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-comment-dots fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="my-3">Consulenza</h4>
          <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
        </div>
        <div class="col-md-3">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-pencil-ruler fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="my-3">Progettazione</h4>
          <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
        </div>
        <div class="col-md-3">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-hammer fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="my-3">Realizzazione</h4>
          <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
        </div>
        <div class="col-md-3">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-water fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="my-3">Manutenzione</h4>
          <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
        </div>
      </div>
    </div>
  </section>
  
    <div class="container-flex bg-secondary">
      <div class="row justify-content-center">
        <h2 class="text-center mt-5">PORTFOLIO</h2>
      </div>

      <div class="container-flex bg-secondary">
        <div class="row justify-content-center mb-5">
          <h4 class="text-center ">Progettiamo realizziamo e ci prendiamo cura del tuo benessere</h4>
        </div>
 
      <div class="row justify-content-center">
        <div class="col-3">
          <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="img/home.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Piscine a sfioro</h5>
          </div>
        </div>
      </div>

      <div class="col-3">
        <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="img/home.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Piscine a sfioro</h5>
        </div>
      </div>
    </div>

    <div class="col-3">
          <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="img/home.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Piscine a sfioro</h5>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center mt-5 ">
      <div class="col-3">
        <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="img/home.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Piscine a sfioro</h5>
        </div>
      </div>
    </div>

    <div class="col-3">
      <div class="card" style="width: 18rem;">
      <img class="card-img-top" src="img/home.jpg" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Piscine a sfioro</h5>
      </div>
    </div>
  </div>

  <div class="col-3">
        <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="img/home.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Piscine a sfioro</h5>
        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-center">
    <h2 class="text-center mt-5 mb-5"></h2>
  </div>
  </div>


  



  <footer class="bg-primary text-white text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
          <h5 class="text-uppercase">Footer Content</h5>
  
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste atque ea quis
            molestias. Fugiat pariatur maxime quis culpa corporis vitae repudiandae aliquam
            voluptatem veniam, est atque cumque eum delectus sint!
          </p>
        </div>
        <!--Grid column-->
  
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Links</h5>
  
          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
  
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase mb-0">Links</h5>
  
          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white">Contatti</a>
            </li>
            <li>
              <a href="#!" class="text-white">Chi siamo</a>
            </li>
            <li>
              <a href="#!" class="text-white">Dove siamo</a>
            </li>
            <li>
              <a href="#!" class="text-white">Prodotti</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->
  
    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
      © 2020 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>

  

  

  

  

 
  
  <script src="https://kit.fontawesome.com/8bac0a4595.js" crossorigin="anonymous"></script>
  
  @endsection
  